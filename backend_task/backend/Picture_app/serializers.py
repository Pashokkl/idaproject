from rest_framework import serializers
from .models import Picture


class ImageSerializer (serializers.ModelSerializer):
    """
    Сериализатор для Получение списка доступных изображений/ Для добавления нового изображения
    """
    class Meta:
        model = Picture
        fields = ('id','name', 'image')


class ImageSerializerDetail (serializers.ModelSerializer):
    """
    Сериализатор для Получение детальной информации о изображении
    """
    class Meta:
        model = Picture
        fields = ('id', 'width', 'top')
        read_only_fields = ('width', 'top')


class ImageSerializerResize (serializers.ModelSerializer):
    """
    Сериализатор для Изменение размера изображения
    """
    def update(self, instance, validated_data):
        instance.id = validated_data.get("id", instance.id)
        instance.name = validated_data.get("name", instance.name)
        instance.width = validated_data.get("width", instance.width)
        instance.top = validated_data.get("top", instance.top)
        instance.save()
        return instance

    class Meta:
        model = Picture
        fields = ('id', 'width', 'top')
