import json
from PIL import Image
from django.http import HttpResponse
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Picture
from .serializers import ImageSerializer, ImageSerializerDetail, ImageSerializerResize
from rest_framework import status, generics, request


class ImageViewSet(ListAPIView):
    """
    Вьюха для Получение списка доступных изображений/ Для добавления нового изображения
    """
    permission_classes = (AllowAny,)
    serializer_class = ImageSerializer

    def post(self, request, *args, **kwargs):
        file = request.data['image']
        image_file = Picture.objects.create(image=file)
        image = Image.open(image_file.image.file)
        width, top = image.size
        image_file.width = width
        image_file.top = top
        image_file.name = request.data['name']
        image_file.save()
        return HttpResponse(json.dumps({'message': "Uploaded"}), status=201)

    def get_queryset(self):
        queryset = Picture.objects.all()
        return queryset


class ImageDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Вьюха для Получение детальной информации о изображении/ Удаление указанного изображения
    """
    permission_classes = (AllowAny,)
    serializer_class = ImageSerializerDetail
    queryset = Picture.objects.all()
    lookup_field = 'id'

    def get_queryset(self):
        return self.queryset

    def delete(self, request, *args, **kwargs):
        pk = kwargs.get("id", None)
        if not pk:
            return Response({"error": "Method DELETE not allowed"})

        instance = Picture.objects.get(id=pk)
        instance.delete()

        return Response({"post": "delete post " + str(pk)})


class ImageResizeViewSet(APIView):
    """
    Вьюха для Изменение размера изображения
    """
    permission_classes = (AllowAny,)
    serializer_class = ImageSerializerResize
    queryset = Picture.objects.all()
    lookup_field = 'id'

    def put(self, request, *args, **kwargs):
        pk = kwargs.get("id", None)
        if not pk:
            return Response({"error": "Method PUT not allowed"})

        try:
            instance = Picture.objects.get(id=pk)
        except:
            return Response({"error": "Object does not exists"})

        serializer = ImageSerializerResize(data=request.data, instance=instance)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({"post": serializer.data})