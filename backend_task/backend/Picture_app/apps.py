from django.apps import AppConfig


class PictureAppConfig(AppConfig):
    name = 'Picture_app'
