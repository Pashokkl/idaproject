from django.urls import include, path
import Picture_app.views as views


urlpatterns = [
    path('images/<int:id>',
         views.ImageDetail.as_view(),
         name='image-detail'),
    path('images/<int:id>/resize',
         views.ImageResizeViewSet.as_view(),
         name='image-resize'),
    path('images', views.ImageViewSet.as_view(), name='images'),
]


