from django.db import models


def nameFile(instance, filename):
    return '/'.join(['images', str(instance.name), filename])

class Picture(models.Model):
    name = models.CharField(max_length=150)
    image = models.ImageField(upload_to='images/', blank=True, null=True)
    width = models.IntegerField(null=True,
                                     default=None,
                                     verbose_name='Ширина картинки')
    top = models.IntegerField(null=True,
                                     default=None,
                                     verbose_name='Высота картинки')

    def __str__(self):
        return self.id